import { json, urlencoded, Router, Request, Response } from 'express';
import dotenv from 'dotenv';
import session from 'express-session';

dotenv.config({ path: 'dev.env' });
const router = Router();

// middlewares
router
    .use(json())
    .use(urlencoded({ extended: false }))
    .use(
        session({
            name: 'isLoggedIn',
            secret: 'secret',
            resave: false,
            saveUninitialized: false,
            cookie: {
                // eslint-disable-next-line no-magic-numbers
                maxAge: 60 * 60 * 24,
                httpOnly: true
            }
        })
    );

router
    .post('/api/auth/sign_in', (req: Request, res: Response) => {
        const user = req.body;
        if (user.password === process.env.USER_PASSWORD) {
            // @ts-ignore
            req.session.user = user;
            res.send({
                status: true,
                response: [true, { data: null, error: '' }]
            });
        } else {
            res.status(404).send({ status: false });
        }
    })
    .post('/api/auth/get_code', (_: Request, res: Response) => {
        res.send({ status: true, errors: [{ phone: ['Wrong phone format!'] }] });
        // res.send({ status: false, errors: [{ phone: ['Wrong phone format!'] }] });
    });

export default router;
