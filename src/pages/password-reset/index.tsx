import React from 'react';
import { Formik, Field, Form } from 'formik';
import MaskedInput from 'react-input-mask';
import { useDispatch, useSelector } from 'react-redux';

import { AuthLayout } from '../../components/auth-layout';
import { validatePhone } from '../authorization/validate';
import { withAuthRoute } from '../../hocs/with-auth-route';
import { resetPassword, clearResetPasswordStatus } from '../../store/ducks/authorization/actions';
import { resetPasswordStatus } from '../../store/ducks/authorization/selectors';
import { Button } from '../../components/button';

import style from './style.module.css';

export interface Values {
    phone: string;
}

const PasswordResetPage = () => {
    const dispatch = useDispatch();
    const resetStatus = useSelector(resetPasswordStatus);

    React.useEffect(() => {
        return () => {
            dispatch(clearResetPasswordStatus());
        };
    }, [dispatch]);

    const onSubmit = React.useCallback((values: Values) => {
        dispatch(resetPassword(values));
    }, [dispatch]);

    if (resetStatus) {
        return (
            <AuthLayout>
                <h2 className={style.title}>Пароль для логина сброшен</h2>
                <Button as="link" to="/">Перейти к форме входа</Button>
            </AuthLayout>
        );
    }

    return (
        <AuthLayout>
            <Formik
                initialValues={{
                    phone: ''
                }}
                validateOnChange={false}
                validateOnBlur={false}
                onSubmit={onSubmit}
                validate={validatePhone}
            >
                {({ errors }) => (
                    <Form>
                        <div className={style.item}>
                            <label htmlFor="phone">Логин</label>
                            <Field name="phone">
                                {({ field }) => (
                                    <MaskedInput
                                        {...field}
                                        id="phone"
                                        className={style.input}
                                        mask="+7 (999) 999-99-99"
                                        placeholder="Введите ваш логин"
                                    />
                                )}
                            </Field>
                            {errors.phone && <span className={style.error}>{errors.phone}</span>}
                        </div>
                        <Button type="submit">Сбросить пароль</Button>
                    </Form>
                )}
            </Formik>
        </AuthLayout>
    );
};

export const PasswordReset = withAuthRoute(PasswordResetPage);
