import React from 'react';

import { withPrivateRoute } from '../../hocs/with-private-route';

const DashboardPage = () => (
    <>
        <h1>Dashboard</h1>
    </>
);

export const Dashboard = withPrivateRoute(DashboardPage);
