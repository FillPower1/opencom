import React from 'react';
import { Formik, Field, Form } from 'formik';
import MaskedInput from 'react-input-mask';
import { useDispatch } from 'react-redux';

import { AuthLayout } from '../../components/auth-layout';
import { signIn } from '../../store/ducks/authorization/actions';
import { withAuthRoute } from '../../hocs/with-auth-route';
import { LinkButton } from '../../components/link-button';
import { Button } from '../../components/button';

import { validate } from './validate';
import style from './style.module.css';

export interface Values {
    phone: string;
    password: string;
}

const AuthorizationPage = () => {
    const dispatch = useDispatch();

    const onSubmit = React.useCallback((values: Values) => {
        dispatch(signIn(values));
    }, [dispatch]);

    return (
        <AuthLayout>
            <Formik
                initialValues={{
                    phone: '',
                    password: ''
                }}
                validateOnChange={false}
                validateOnBlur={false}
                onSubmit={onSubmit}
                validate={validate}
            >
                {({ errors }) => (
                    <Form>
                        <div className={style.item}>
                            <label htmlFor="phone">Логин</label>
                            <Field name="phone">
                                {({ field }) => (
                                    <MaskedInput
                                        {...field}
                                        className={style.input}
                                        mask="+7 (999) 999-99-99"
                                        id="phone"
                                        placeholder="Введите ваш логин"
                                    />
                                )}
                            </Field>
                            {errors.phone && <span className={style.error}>{errors.phone}</span>}
                        </div>
                        <div className={style.item}>
                            <label htmlFor="password">Пароль</label>
                            <Field
                                id="password"
                                className={style.input}
                                type="password"
                                name="password"
                                placeholder="Введите ваш пароль"
                            />
                            {errors.password && <span className={style.error}>{errors.password}</span>}
                        </div>
                        <Button as="button" type="submit">Войти</Button>
                        <LinkButton to="/password_reset">Забыли пароль</LinkButton>
                    </Form>
                )}
            </Formik>
        </AuthLayout>
    );
};

export const Authorization = withAuthRoute(AuthorizationPage);
