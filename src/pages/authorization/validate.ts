import { clearPhone } from '../../utils/clear-phone';

import { Values } from './';

const PASSWORD_LENGTH = 8;
const PHONE_LENGTH = 11;

interface Errors {
    [key: string]: string
}

export const validatePhone = ({ phone = '' }: { phone: string }) => {
    if (!phone) {
        return {
            phone: 'Обязательное поле'
        };
    }

    const phoneLength = clearPhone(phone).length;
    if (phoneLength > 0 && phoneLength !== PHONE_LENGTH) {
        return {
            phone: 'Некорректный телефон'
        };
    }

    return {};
};

export const validate = (values: Values) => {
    const errors: Errors = {};

    const phone = validatePhone(values);

    if (values.password.trim().length < PASSWORD_LENGTH) {
        errors.password = 'Минимальная длина пароля 8 символов';
    }

    return {
        ...errors,
        ...phone
    };
};
