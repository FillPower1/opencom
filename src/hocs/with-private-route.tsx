import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { isAuthorized } from '../store/ducks/authorization/selectors';

export const withPrivateRoute = (Component: any) => (props: any) => {
    const isLoggedIn = useSelector(isAuthorized);

    if (!isLoggedIn) {
        return <Redirect to="/" />;
    }

    return <Component {...props} />;
};
