import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { isAuthorized } from '../store/ducks/authorization/selectors';

export const withAuthRoute = (Component: any) => (props: any) => {
    const isLoggedIn = useSelector(isAuthorized);

    if (isLoggedIn) {
        return <Redirect to="/dashboard" />;
    }

    return <Component {...props} />;
};
