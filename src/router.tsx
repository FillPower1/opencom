import React from 'react';
import { BrowserRouter as ReactRouter, Switch, Route } from 'react-router-dom';

import { NotFound } from './components/not-found';
import { Authorization } from './pages/authorization';
import { PasswordReset } from './pages/password-reset';
import { Dashboard } from './pages/dashboard';

export const Router = () => (
    <ReactRouter>
        <Switch>
            <Route path="/" exact component={Authorization} />
            <Route path="/dashboard" exact component={Dashboard} />
            <Route path="/password_reset" exact component={PasswordReset} />
            <Route path="*" component={NotFound} />
        </Switch>
    </ReactRouter>
);
