import React from 'react';
import { Link } from 'react-router-dom';
import cn from 'classnames';

import style from './style.module.css';

interface Props {
    as?: 'link' | 'button',
    children: React.ReactNode,
    [key: string]: any
}

export const Button = ({ as, children, ...props }: Props) => {
    if (as === 'link') {
        return (
            <Link
                {...props}
                to={props.to}
                className={cn(style.button, style.primary)}
            >
                {children}
            </Link>
        );
    }

    return (
        <button
            {...props}
            className={cn(style.button, style.primary)}
        >
            {children}
        </button>
    );
};

Button.defaultProps = {
    as: 'button'
};
