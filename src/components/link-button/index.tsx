import React from 'react';
import { Link } from 'react-router-dom';
import cn from 'classnames';

import style from './style.module.css';

interface Props {
    children: React.ReactNode,
    [key: string]: any
}

export const LinkButton = ({ as, children, ...props }: Props) => (
    <Link
        {...props}
        to={props.to}
        className={cn(style.button, style.link)}
    >
        {children}
    </Link>
);
