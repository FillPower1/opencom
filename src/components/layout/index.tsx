import React from 'react';

import style from './style.module.css';

interface Props {
    children: React.ReactNode
}

export const Layout = ({ children }: Props) => (
    <div className={style.layout}>
        {children}
    </div>
);
