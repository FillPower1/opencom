import React from 'react';

import Logo from '../../assets/logo.svg';

import topBarBg from './top_bar_bg.jpg';
import style from './style.module.css';

export const AuthLayout = ({ children }) => (
    <div className={style.wrapper}>
        <div className={style.formWrapper}>
            <span className={style.logo}><Logo /></span>
            {children}
        </div>
        <div className={style.background}>
            <img src={topBarBg} alt="" />
        </div>
    </div>
);
