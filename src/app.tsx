import React from 'react';

import { Layout } from './components/layout';
import { Router } from './router';

export const App = () => (
    <Layout>
        <Router />
    </Layout>
);
