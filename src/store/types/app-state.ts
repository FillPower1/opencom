import { AuthorizationState } from '../ducks/authorization/reducer';

export interface State {
    readonly authorization: AuthorizationState;
}
