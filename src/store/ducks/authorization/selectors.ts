import { State } from '../../types/app-state';

export const isAuthorized = (state: State) => state.authorization.isAuthorized;
export const resetPasswordStatus = (state: State) => state.authorization.resetPasswordStatus;
