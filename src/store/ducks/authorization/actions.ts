import { createAsyncThunk, createAction } from '@reduxjs/toolkit';

import axios from '../../axios';
import { clearPhone } from '../../../utils/clear-phone';

interface Values {
    phone: string;
    password: string;
}

export const signIn = createAsyncThunk(
    'authorization/success',
    async (form: Values) => {
        const loginForm = {
            ...form,
            phone: clearPhone(form.phone)
        };

        const { data } = await axios({
            method: 'POST',
            url: '/api/auth/sign_in',
            data: loginForm
        });

        return data;
    }
);

export const resetPassword = createAsyncThunk(
    'authorization/reset',
    async (form: { phone: string }) => {
        const { data } = await axios({
            method: 'POST',
            url: '/api/auth/get_code',
            data: {
                phone: clearPhone(form.phone)
            }
        });

        return data;
    }
);

export const clearResetPasswordStatus = createAction('authorization/clearResetPasswordStatus');
