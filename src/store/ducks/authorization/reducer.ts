import { createReducer, PayloadAction } from '@reduxjs/toolkit';

import { clearResetPasswordStatus, resetPassword, signIn } from './actions';

export interface AuthorizationState {
    isAuthorized: boolean;
    hasError: boolean;
    resetPasswordStatus: boolean;
}

interface Payload {
    status: boolean;
    response: any[]
}

export const initialState = {
    isAuthorized: false,
    hasError: false,
    resetPasswordStatus: false
} as AuthorizationState;

export default createReducer(initialState, builder => {
    builder.addCase(signIn.fulfilled, (state, action: PayloadAction<Payload>) => {
        const { status } = action.payload;
        state.isAuthorized = status;
    });
    builder.addCase(signIn.rejected, state => {
        state.isAuthorized = false;
        state.hasError = true;
    });

    builder.addCase(resetPassword.fulfilled, state => {
        state.resetPasswordStatus = true;
    });
    builder.addCase(resetPassword.rejected, state => {
        state.resetPasswordStatus = false;
    });
    builder.addCase(clearResetPasswordStatus, state => {
        state.resetPasswordStatus = false;
    });
});
