import { configureStore } from '@reduxjs/toolkit';

import authorization from './ducks/authorization/reducer';

export const store = configureStore({
    reducer: {
        authorization
    },
    devTools: process.env.NODE_ENV !== 'production'
});
