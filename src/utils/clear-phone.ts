export const clearPhone = (phone: string) => phone.replace(/[-+_()\s]/g, '');
