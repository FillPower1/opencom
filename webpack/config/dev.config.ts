import path from 'path';

import webpack from 'webpack';
import { merge } from 'webpack-merge';

import { ROOT } from '../dir';
import router from '../../stub';

import commonConfig from './base.config';

export default merge(
    commonConfig,
    {
        target: 'web',
        output: {
            publicPath: '/',
            filename: '[name].js'
        },
        mode: 'development',
        module: {
            rules: [
                {
                    test: /\.css$/,
                    use: [
                        'style-loader',
                        'css-loader',
                        'postcss-loader'
                    ],
                    exclude: /\.module\.css$/
                },
                {
                    test: /\.css$/,
                    use: [
                        'style-loader',
                        {
                            loader: 'css-loader',
                            options: {
                                modules: {
                                    localIdentName: '[path][name]__[local]--[hash:base64:5]',
                                    exportLocalsConvention: 'camelCase',
                                    exportOnlyLocals: false
                                },
                                sourceMap: true
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: true
                            }
                        }
                    ],
                    include: /\.module\.css$/
                }
            ]
        },
        devServer: {
            contentBase: path.join(ROOT, 'dist'),
            port: 4000,
            hot: true,
            historyApiFallback: true,
            before(app) {
                app.use(router);
            }
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin()
        ],
        performance: {
            hints: false
        },
        devtool: 'source-map'
    } as webpack.Configuration
);
