import webpack from 'webpack';
import { TsconfigPathsPlugin } from 'tsconfig-paths-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';

import { DIST_DIR, SOURCE_DIR } from '../dir';
import { GLOBAL_ARGS, ENVS } from '../env';

const { __DEV__ } = ENVS;

export default {
    entry: SOURCE_DIR,
    output: {
        path: DIST_DIR,
        publicPath: '/',
    },
    mode: 'none',
    module: {
        rules: [
            {
                test: /\.ts|js(x?)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'assets/[name]-[hash:5].[ext]'
                        }
                    }
                ]
            },
            {
                test: /\.(woff(2)?|eot|ttf|otf|)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'fonts/[name].[ext]'
                        }
                    }
                ]
            },
            {
                test: /\.svg$/,
                use: ['@svgr/webpack'],
            }
        ]
    },
    stats: {
        all: undefined,
        builtAt: !__DEV__,
        chunks: !__DEV__,
        assets: !__DEV__,
        errors: true,
        warnings: true,
        outputPath: true,
        timings: true,
        children: false
    },
    resolve: {
        modules: ['src', 'node_modules'],
        extensions: ['*', '.js', '.jsx', '.json', '.ts', '.tsx'],
        plugins: [new TsconfigPathsPlugin()]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'static/index.html'
        }),
        new webpack.DefinePlugin(GLOBAL_ARGS)
    ],
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendors: {
                    name: 'vendors',
                    test: /node_modules/,
                    chunks: 'all',
                    enforce: true
                }
            }
        }
    }
} as webpack.Configuration;
