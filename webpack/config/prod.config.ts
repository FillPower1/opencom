import webpack from 'webpack';
import { merge } from 'webpack-merge';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import TerserPlugin from 'terser-webpack-plugin';

import commonConfig from './base.config';

export default merge(
    commonConfig,
    {
        output: {
            publicPath: '/',
            filename: '[name].[contenthash].js',
            hashDigestLength: 8
        },
        mode: 'production',
        module: {
            rules: [
                {
                    test: /\.css$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                            options: {
                                modules: {
                                    localIdentName: '[hash:base64:8]',
                                    exportLocalsConvention: 'camelCase',
                                    exportOnlyLocals: false
                                },
                                sourceMap: false
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: false
                            }
                        }
                    ]
                }
            ]
        },
        plugins: [
            new CleanWebpackPlugin(),
            new MiniCssExtractPlugin({
                filename: '[name].[contenthash:5].css',
                chunkFilename: '[name].[contenthash:5].css'
            })
        ],
        performance: {
            hints: 'warning'
        },
        devtool: false,
        optimization: {
            minimize: true,
            minimizer: [new TerserPlugin({
                terserOptions: {
                    output: {
                        comments: false
                    }
                }
            })]
        }
    } as webpack.Configuration
);
