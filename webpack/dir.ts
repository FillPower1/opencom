import path from 'path';

import cwd from 'cwd';

const ROOT = cwd();
const DIST_DIR = path.join(ROOT, 'dist');
const SOURCE_DIR = path.join(ROOT, 'src/index.tsx');

export { ROOT, DIST_DIR, SOURCE_DIR };
